#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#define HEAP_SIZE (1024 * 1024) // 1 megabyte

// All functions like memoryCopy, writeMemory, etc return this constants
#define OK 0
#define NOT_OK 1

// Just for convenience
typedef unsigned int uint;

typedef struct
{
    uint address;
    uint length;

} MemoryBlock;

typedef struct
{
    MemoryBlock* blocks;
    uint blockCount;

} MemoryMap;

typedef struct 
{
    uint size;

    char* memory;
    MemoryMap* memoryMap;

} Heap;

// Returns the last block of memory in heap
MemoryBlock* getLastBlock(Heap* heap)
{
    return &heap->memoryMap->blocks[heap->memoryMap->blockCount - 1];
}

void initializeHeap(Heap* heap, uint heapSize)
{
    heap->memory = (char*)malloc(heapSize);
    heap->memoryMap = (MemoryMap*)malloc(sizeof(MemoryMap));
    heap->size = heapSize;

    heap->memoryMap->blockCount = 0;
    heap->memoryMap->blocks = NULL;
}

// Allocates memory in the heap
// Returns address of the first byte of allocated block
// If allocation failed, returns 0
uint allocateMemory(Heap* heap, uint size)
{
    uint currentEnd = 1;
    uint newBlockStart = heap->size;
    uint i;

    // Calculating the free space between blocks
    for (i = 0; i < heap->memoryMap->blockCount; i++)
    {
        uint freeSpace = heap->memoryMap->blocks[i].address - currentEnd;

        // If this block of free memory is larger
        // than allocation size, memorize it 
        if (freeSpace >= size)
        {
            newBlockStart = currentEnd;
            break;
        } 

        currentEnd = heap->memoryMap->blocks[i].address + heap->memoryMap->blocks[i].length;
    }

    // If we have no blocks, then just allocate memory
    // at the beginning of the heap
    if (heap->memoryMap->blockCount == 0)
        newBlockStart = 1;

    MemoryBlock* lastBlock = getLastBlock(heap);

    // If we haven't found any block of free memory,
    // check the space of memory after all blocks
    if (newBlockStart == heap->size &&
        heap->memoryMap->blockCount > 0 && 
        heap->size - lastBlock->address - lastBlock->length + 1 >= size)
    {
        newBlockStart = lastBlock->address + lastBlock->length;
    }

    // Finally, after all previous manipulations,
    // if we still have not found matching block of free memory,
    // return 0 (means that memory was not allocated)
    if (newBlockStart == heap->size)
        return 0;

    // If we reach this code, we definitely have free 
    // block of memory that can be allocated 

    // Increment number of blocks in heap
    heap->memoryMap->blockCount++;
    heap->memoryMap->blocks = (MemoryBlock*)realloc(heap->memoryMap->blocks, 
        heap->memoryMap->blockCount * sizeof(MemoryBlock));

    lastBlock = getLastBlock(heap);
    lastBlock->address = newBlockStart;
    lastBlock->length = size;

    // To work properly, all blocks in heap->memoryMap->blocks
    // must be sorted by their addresses
    // Here we using insertion sort algorithm to quickly insert new block
    i = heap->memoryMap->blockCount - 2;
    while (i != UINT_MAX && newBlockStart < heap->memoryMap->blocks[i].address)
    { 
        MemoryBlock temp = heap->memoryMap->blocks[i];
        heap->memoryMap->blocks[i] = heap->memoryMap->blocks[i + 1];
        heap->memoryMap->blocks[i + 1] = temp;

        i--;
    }

    return newBlockStart;
}

MemoryBlock findBlockByAddress(Heap* heap, uint address, int* index)
{
    MemoryBlock mb = { 0, 0 };

    // Iterating through all blocks
    for (uint i = 0; i < heap->memoryMap->blockCount; i++)
        // If given address is inside current block - return it
        if (address >= heap->memoryMap->blocks[i].address &&
            address < heap->memoryMap->blocks[i].address + heap->memoryMap->blocks[i].length)
        {
            mb.address = heap->memoryMap->blocks[i].address;
            mb.length = heap->memoryMap->blocks[i].length;

            if (index != NULL)
                *index = i;

            break;
        }

    return mb;
}

int writeMemory(Heap* heap, uint address, char* data, uint bytes)
{
    MemoryBlock foundBlock = findBlockByAddress(heap, address, NULL);

    if (foundBlock.address == 0)
        return NOT_OK;

    // If we try to write more than block length
    if (bytes > foundBlock.length)
        bytes = foundBlock.length;

    // If given address is not the first byte of block,
    // reduce number of bytes to write
    bytes -= address - foundBlock.address;

    memcpy(heap->memory + address, data, bytes);

    return OK;
}

int readMemory(Heap* heap, uint address, char* buffer)
{
    MemoryBlock foundBlock = findBlockByAddress(heap, address, NULL);

    if (foundBlock.address == 0)
        return NOT_OK;

    memcpy(buffer, heap->memory + foundBlock.address, foundBlock.length);

    return OK;
}

int freeMemory(Heap* heap, uint address)
{
    int foundBlockIndex;
    MemoryBlock foundBlock = findBlockByAddress(heap, address, &foundBlockIndex);

    if (foundBlock.address == 0)
        return NOT_OK;

    // Deleting item from heap->memoryMap->blocks
    memmove(heap->memoryMap->blocks + foundBlockIndex, 
        heap->memoryMap->blocks + foundBlockIndex + 1,
        heap->memoryMap->blockCount - foundBlockIndex - 1);

    // Reallocating block array
    heap->memoryMap->blockCount--;
    heap->memoryMap->blocks = (MemoryBlock*)realloc(heap->memoryMap->blocks, 
        heap->memoryMap->blockCount * sizeof(MemoryBlock));

    return OK;
}

int copyMemory(Heap* heap, uint destination, uint source, uint bytes)
{
    MemoryBlock sourceBlock = findBlockByAddress(heap, source, NULL);
    
    if (sourceBlock.address == 0)
        return NOT_OK;

    char buffer[sourceBlock.length];

    readMemory(heap, source, buffer);
    writeMemory(heap, destination, buffer, bytes);

    return OK;
}

int moveMemory(Heap* heap, uint destination, uint source, uint bytes)
{
    if (copyMemory(heap, destination, source, bytes) == NOT_OK)
        return NOT_OK;

    MemoryBlock sourceBlock = findBlockByAddress(heap, source, NULL);
    memset(heap->memory + sourceBlock.address, 0, sourceBlock.length);

    return OK;
}

void freeHeap(Heap* heap)
{
    free(heap->memory);
    free(heap->memoryMap->blocks);
    free(heap->memoryMap);
    free(heap);
}

int main()
{
    Heap* heap = (Heap*)malloc(sizeof(Heap));
    initializeHeap(heap, HEAP_SIZE);

    uint a = allocateMemory(heap, sizeof(int));
    int number1 = 42, number2;
    long number3;

    writeMemory(heap, a, (char*)&number1, sizeof(int));

    readMemory(heap, a, (char*)&number2);
    printf("%i\n", number2);

    uint b = allocateMemory(heap, sizeof(long));
    copyMemory(heap, b, a, sizeof(int));

    readMemory(heap, b, (char*)&number3);
    printf("%li\n", number3);

    freeMemory(heap, a);
    freeHeap(heap);
}